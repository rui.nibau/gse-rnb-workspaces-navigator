# rnb-workspaces-navigator

Simple workspaces navigator for gnome-shell

• [Website](https://omacronides.com/projets/gse-rnb-workspaces-navigator/)
• [Source](https://framagit.org/rui.nibau/gse-rnb-workspaces-navigator)
• [Issues](https://framagit.org/rui.nibau/gse-rnb-workspaces-navigator/-/issues)

Project is canceled, replaced by [rnb-panel](https://omacronides.com/projets/gse-rnb-panel/)