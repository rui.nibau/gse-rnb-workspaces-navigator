---
title:      Historique
date:       2021-04-06
updated:    2023-04-22
---

°°changelog°°
1.2.0 øø(2023-04-22)øø:
    • upd: Gnome 44 compatibility
    • upd: Active window
1.1.0 øø(2022-10-15)øø:
    • upd: Gnome 43 compatibility
    • add: Focus on the window of the current workspace
1.0.0 øø(2022-04-10)øø:
    • add: First publication

