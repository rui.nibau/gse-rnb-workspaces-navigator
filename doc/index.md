---
title:      gse : rnb-workspaces-navigator
banner:     /lab/gse-rnb-workspaces-navigator/pics/1.0.0.jpg
date:       2021-04-06
updated:    2023-04-22
cats:       [informatique]
tags:       [gnome-shell, extensions]
techs:      [javascript, gnome]
version:    1.2.0
source:     https://framagit.org/rui.nibau/gse-rnb-workspaces-navigator
issues:     https://framagit.org/rui.nibau/gse-rnb-workspaces-navigator/-/issues
download:   /lab/gse-rnb-workspaces-navigator/build/latest/rnb-workspaces-navigator.zip
itemtype:   SoftwareApplication
intro: Une extension gnome-shell 40-44 extrêmement simple qui permet (1) de visualiser le nombre de bureaux et (2) de naviguer entre eux par simple scroll au-dessus du Top-panel.
---

## Install from zip

• [Download the zip file](/lab/gse-rnb-workspaces-navigator/build/latest/rnb-workspaces-navigator.zip) and unzip it.
• copy the folder ``rnb-workspaces-navigator@omacronides.com`` into ``~/.local/share/gnome-shell/extensions/``.


## Install from source

Download the sources :

°°stx-bash°°
    # Download sources
    git clone https://framagit.org/rui.nibau/gse-rnb-workspaces-navigator.git
    # Go to directory
    cd gse-rnb-taskbar
    # Checkout version to install
    git checkout tags/<version>
    # Install the extension using npm
    npm run deploy


## Features

• Something simple and non intrusive.
• Switch from one workspace to another by clicking on it.
• Switch between workspaces by scrolling on the main panel.
• Focus on the window of the current workspace.


## What will maybe be in it

• Preferences


## What will not be in it

• Workspace pre-visualization : use overview.
• Scroll outside main panel : Use Gnome keyboard / mouse shortcuts.

If you want more, use [Workspace Indicator](https://gitlab.gnome.org/GNOME/gnome-shell-extensions/-/tree/master/extensions/workspace-indicator).


## Hack

• Edit ``~/.local/share/gnome-shell/extensions/rnb-workspaces-navigator@omacronides.com/stylesheet.css`` to change aspect and colors.

°°todo°°A   écrire...


## Ressources et références

• [Développement javascript sous Gnome](/articles/gjs)
• [Workspace Indicator](https://gitlab.gnome.org/GNOME/gnome-shell-extensions/-/tree/master/extensions/workspace-indicator)


## History

..include::./changelog.md


## Licence

..include::./licence.md

