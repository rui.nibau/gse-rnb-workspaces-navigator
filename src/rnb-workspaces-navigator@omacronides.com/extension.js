import Clutter from 'gi://Clutter';
import GObject from 'gi://GObject';
// import Meta from 'gi://Meta';
import St from 'gi://St';

// const Mainloop = imports.mainloop;

import { Button } from 'resource:///org/gnome/shell/ui/panelMenu.js';
import { Extension } from 'resource:///org/gnome/shell/extensions/extension.js';
import { panel } from 'resource:///org/gnome/shell/ui/main.js';

class WorkspacesNavigatorView extends Button {
    static {
        GObject.registerClass(this);
    }

    _init() {
        super._init({}, 'Workspace Indicator');

        this.add_style_class_name('rnb-workspaces-navigator');
        this.add_style_class_name('rnb-panel');

        /** @type {number[]} */
        this._wsManagerSignalIds = [];

        /** @type {number} */
        this._scrollSignalId = null;

        /** @type {import('gi').St.BoxLayout} */
        this._box = new St.BoxLayout({ y_align: Clutter.ActorAlign.CENTER, y_expand: false });
        this.add_child(this._box);

        this.connect('button-press-event', (actor, event) => {
            const index = event.get_source().workspaceIndex;
            if (index !== undefined) {
                this.activate(index);
            }
        });

        this._connections = new Map([
            [global.workspace_manager, [
                global.workspace_manager.connect_after('workspace-added', () => this.update()),
                global.workspace_manager.connect_after('workspace-removed', () => this.update()),
                global.workspace_manager.connect_after('workspace-switched', () => this.updateActive()),
                global.workspace_manager.connect_after('active-workspace-changed', this._onWsChanged.bind(this)),
            ]],
            [panel, [
                panel.connect_after('scroll-event', this._onScroll.bind(this)),
            ]],
        ]);

        this.update();
    }

    destroy() {
        console.log('WorkspacesNavigatorView.destroy()');
        this._connections.forEach((ids, object) => ids.forEach(id => object.disconnect(id)));
        this._connections.clear();
        super.destroy();
    }

    /**
     * @param {import('gi').Clutter.Actor} actor Where the scroll happens
     * @param {import('gi').Clutter.Event} event Event
     * @returns
     */
    _onScroll(actor, event) {
        const source = event.get_source();

        // scroll on right box : just propagate event
        if (source !== actor && !this._box.contains(source) &&
                panel._rightBox && panel._rightBox.contains &&
                panel._rightBox.contains(source)) {
            return Clutter.EVENT_PROPAGATE;
        }


        // Scroll to workspace
        let diff = 0;
        switch (event.get_scroll_direction()) {
        case Clutter.ScrollDirection.DOWN:
        case Clutter.ScrollDirection.RIGHT:
            // active_workspace.get_neighbor(Meta.MotionDirection.RIGHT).activate(event.get_time());
            diff = 1;
            break;
        case Clutter.ScrollDirection.UP:
        case Clutter.ScrollDirection.LEFT:
            diff = -1;
            break;
        }

        if (diff !== 0) {
            this.activate(global.workspace_manager.get_active_workspace_index() + diff);
        }

        return Clutter.EVENT_STOP;
    }

    /**
     * Puts focus on window in the active workspace.
     *
     * @returns void
     */
    _onWsChanged() {
        //
        // if (!Meta.prefs_get_workspaces_only_on_primary()) {
        //     return;
        // }
        // Mainloop.timeout_add(0, () => {
        //     /** @type {import('gi').Meta.Workspace} */
        //     const activeWs = global.workspaceManager.get_active_workspace();
        //     // https://discourse.gnome.org/t/trying-to-make-focus-change-on-workspace-switch-less-random
        //     const windows = activeWs.list_windows().filter(w => w.is_on_primary_monitor());
        //     if (windows.length > 0) {
        //         const target = windows.reduce((prev, current) => {
        //             if (current.get_user_time() > prev.get_user_time()) {
        //                 return current;
        //             }
        //             return prev;
        //         });
        //         console.log(`activate with focus on: ${target?.get_title()}`);
        //         target?.activate(global.get_current_time());
        //     }
        // });
    }

    /**
     * Activate a workspace
     *
     * @param {number} index of the workspace
     */
    activate(index) {
        if (index >= 0 && index < global.workspace_manager.n_workspaces) {
            console.log(`WorkspacesNavigatorView.activate(${index})`);
            const currentWs = global.workspace_manager.get_active_workspace();
            const newWs = global.workspace_manager.get_workspace_by_index(index);
            if (newWs !== currentWs) {
                newWs.activate(global.get_current_time());
            }
        }
    }

    updateActive() {
        if (this._box !== null) {
            const current = global.workspace_manager.get_active_workspace_index();
            this._box.get_children().forEach((child, i) => {
                if (i === current) {
                    child.add_style_class_name('active');
                } else {
                    child.remove_style_class_name('active');
                }
            });
        }
    }

    update() {
        if (this._box !== null) {
            this._box.destroy_all_children();
            const n = global.workspace_manager.n_workspaces;
            for (let i = 0; i < n; i += 1) {
                const wsButton = new St.Widget({ style_class: 'rnb-workspace', reactive: true });
                wsButton.workspaceIndex = i;
                this._box.add_child(wsButton);
            }
            this.updateActive();
        }
    }
}

export default class WorkspacesNavigator extends Extension {
    /** @type {WorkspacesNavigatorView} */
    instance = null;

    enable() {
        this.instance = new WorkspacesNavigatorView();
        panel.addToStatusArea('rnb-workspaces-navigator', this.instance);
    }

    disable() {
        this.instance?.destroy();
        this.instance = null;
    }
}
